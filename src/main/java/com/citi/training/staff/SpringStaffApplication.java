package com.citi.training.staff;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringStaffApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringStaffApplication.class, args);
	}

}
