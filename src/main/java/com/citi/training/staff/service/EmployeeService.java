package com.citi.training.staff.service;

import java.util.List;

import com.citi.training.staff.dao.EmployeeMongoRepo;
import com.citi.training.staff.model.Department;
import com.citi.training.staff.model.Employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EmployeeService {

    @Autowired
    EmployeeMongoRepo empRepo;

    public Employee save(Employee emp) {
        return empRepo.save(emp);
    }

    public List<Employee> findAll() {
        return empRepo.findAll();
    }

    public List<Employee> findAllByDepartment(Department department){
        return empRepo.findAllByDepartment(department);
    }

    public long count() {
        return empRepo.count();
    }
}
