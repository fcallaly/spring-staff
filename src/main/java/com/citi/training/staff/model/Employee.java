package com.citi.training.staff.model;

public class Employee {
    private String id;
    private String fullname;
    private String code;
    private Department department;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department dept) {
        this.department = dept;
    }

    @Override
    public String toString() {
        return "Employee [code=" + code + ", department=" + department +
               ", fullname=" + fullname + ", id=" + id + "]";
    }
}
