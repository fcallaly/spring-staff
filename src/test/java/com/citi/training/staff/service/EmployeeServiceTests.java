package com.citi.training.staff.service;

import java.util.List;

import com.citi.training.staff.model.Department;
import com.citi.training.staff.model.Employee;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class EmployeeServiceTests {

    private static final Logger LOG = LoggerFactory.getLogger(EmployeeServiceTests.class);
    @Autowired
    EmployeeService empService;

    private static Department salesDept;

    @BeforeAll
    public static void createSalesDepartment(){
        salesDept = new Department();
        salesDept.setName("Sales");
        salesDept.setAddress("123 Main Street");
    }

    @Test
    public void testEmployeeService_save() {
        Employee salesmanEmp = new Employee();
        salesmanEmp.setCode("123ABC");
        salesmanEmp.setFullname("Mr. Test McTest");

        salesmanEmp.setDepartment(salesDept);

        empService.save(salesmanEmp);

        assert(empService.count() > 0);

        for(Employee employee : empService.findAll()) {
            LOG.info(employee.toString());
        }
    }

    @Test
    public void testFindByDepartment() {

        List<Employee> allSales = empService.findAllByDepartment(salesDept);

        assert(allSales.size() > 0);

        for(Employee employee : allSales) {
            LOG.info(employee.toString());
        }
    }
}
